using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTarget : MonoBehaviour
{
    public Transform target;
    public float smoothTime = 0.2f;
    private Vector3 velocity = Vector3.zero;

    void LateUpdate()
    {
        if (target == null) return;

        // Smooth damping movement
        transform.position = Vector3.SmoothDamp(transform.position, target.position, ref velocity, smoothTime);

        // Smooth rotation damping
        transform.rotation = Quaternion.Slerp(transform.rotation, target.rotation, Time.deltaTime * 5f);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
