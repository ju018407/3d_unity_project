using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockerBogieSuspension : MonoBehaviour
{
    public float speed = 10f;            // Speed of the rover
    public float rotationSpeed = 100f;  // Speed of wheel rotation
    public float turnSpeed = 50f;       // Speed of turning (steering)
    public GameObject[] wheels;         // Array to store the wheel GameObjects

    public PhysicMaterial highFrictionMaterial; // High-friction physic material

    // Suspension-related variables
    public Transform frontLeftSuspension;  // Suspension piece for the front left wheel
    public Transform frontLeftWheel;       // Front left wheel for raycasting
    public Transform frontRightSuspension; // Suspension piece for the front right wheel
    public Transform frontRightWheel;      // Front right wheel for raycasting

    // Rear-left suspension piece and wheels
    public Transform rearLeftSuspension;   // Suspension piece for the middle and rear left wheels
    public Transform middleLeftWheel;      // Middle left wheel for raycasting
    public Transform rearLeftWheel;        // Rear left wheel for raycasting

    // Rear-right suspension piece and wheels
    public Transform rearRightSuspension;  // Suspension piece for the middle and rear right wheels
    public Transform middleRightWheel;     // Middle right wheel for raycasting
    public Transform rearRightWheel;       // Rear right wheel for raycasting

    public float suspensionMaxAngle = 30f; // Max angle of the suspension
    public float raycastDistance = 1f;     // Distance to detect terrain
    public float suspensionDamping = 5f;   // Smooth the suspension movement

    public float steeringAngle = 30f;      // Maximum angle for wheel steering
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        // Apply high-friction material to all wheels
        foreach (GameObject wheel in wheels)
        {
            Collider wheelCollider = wheel.GetComponent<Collider>();
            if (wheelCollider != null && highFrictionMaterial != null)
            {
                wheelCollider.material = highFrictionMaterial;
            }
        }
    }

    void FixedUpdate()
    {
        Vector3 downForce = -transform.up * 10f; // Adjust the multiplier as needed
        rb.AddForce(downForce, ForceMode.Acceleration);
        float tiltAngle = Vector3.Angle(Vector3.up, transform.up);
        if (tiltAngle > 30f) // Threshold angle
        {
            Vector3 torque = Vector3.Cross(transform.up, Vector3.up) * 10f;
            rb.AddTorque(torque, ForceMode.Acceleration);
        }
    }

    void Update()
    {
        if (CameraSwitcher.Instance != null && CameraSwitcher.Instance.roverHealth <= 0)
        {
            return; // Stop processing movement inputs
        }

        if (RoverController.Instance != null && RoverController.Instance.autoNavigate)
            return;

        if (CommandPrompt.isInputActive) return;

        // Get input for forward/backward movement and turning
        float moveInput = Input.GetAxis("Vertical");
        float turnInput = Input.GetAxis("Horizontal");

        // Move the rover forward or backward
        Vector3 movement = transform.forward * moveInput * speed * Time.deltaTime;
        rb.MovePosition(rb.position + movement);

        // Rotate the rover for left/right turning
        if (moveInput != 0 && turnInput != 0)
        {
            float adjustedTurnSpeed = turnSpeed * (moveInput < 0 ? -1 : 1); // Reverse turning direction when moving backward
            Quaternion turnRotation = Quaternion.Euler(0f, turnInput * adjustedTurnSpeed * Time.deltaTime, 0f);
            rb.MoveRotation(rb.rotation * turnRotation);
        }

        // Rotate the wheels on their local X-axis based on the movement
        RotateWheels(moveInput);

        // Update steering angle for the front wheels
        SteerWheels(turnInput);

        // Update suspensions dynamically
        AdjustSuspension(frontLeftSuspension, frontLeftWheel);
        AdjustSuspension(frontRightSuspension, frontRightWheel);
        AdjustRearLeftSuspension();
        AdjustRearRightSuspension();
    }

    void OnCollisionEnter(Collision collision)
    {
        // Check if the collided object is in the "rock" layer
        if (collision.gameObject.layer == LayerMask.NameToLayer("Rock"))
        {
            // Get a reference to the CameraSwitcher (assumes there's only one in the scene)
            CameraSwitcher cs = FindObjectOfType<CameraSwitcher>();
            if (cs != null)
            {
                // Reduce health by 1, ensuring it doesn't go below 0
                cs.roverHealth = Mathf.Max(0, cs.roverHealth - 1);
            }
        }
    }


    // Function to rotate all wheels
    public void RotateWheels(float moveInput)
    {
        float tractionControlFactor = 1f;
        if (rb.velocity.magnitude > 10f) // If speed exceeds a threshold
        {
            tractionControlFactor = 0.5f; // Reduce wheel rotation speed
        }

        float rotationAmount = moveInput * rotationSpeed * tractionControlFactor * Time.deltaTime;
        foreach (GameObject wheel in wheels)
        {
            wheel.transform.Rotate(Vector3.right, rotationAmount);
        }
    }

    // Function to steer the front wheels
    public void SteerWheels(float turnInput)
    {
        float targetSteeringAngle = turnInput * steeringAngle;

        Quaternion leftTargetRotation = Quaternion.Euler(0f, 0f, targetSteeringAngle);
        Quaternion rightTargetRotation = Quaternion.Euler(0f, 0f, targetSteeringAngle);

        frontLeftWheel.parent.localRotation = Quaternion.Lerp(frontLeftWheel.parent.localRotation, leftTargetRotation, Time.deltaTime * suspensionDamping);
        frontRightWheel.parent.localRotation = Quaternion.Lerp(frontRightWheel.parent.localRotation, rightTargetRotation, Time.deltaTime * suspensionDamping);
    }

    // Adjust suspension dynamically for a given suspension piece and wheel
    public void AdjustSuspension(Transform suspension, Transform wheel)
    {
        RaycastHit hit;

        // Cast a ray from the wheel downward
        if (Physics.Raycast(wheel.position, Vector3.down, out hit, raycastDistance))
        {
            // Calculate suspension angle based on hit distance
            float distanceToGround = hit.distance;
            float normalizedDistance = Mathf.Clamp01(distanceToGround / raycastDistance);
            float targetAngle = Mathf.Lerp(-suspensionMaxAngle, suspensionMaxAngle, normalizedDistance);

            // Smoothly rotate the suspension to the target angle
            Quaternion targetRotation = Quaternion.Euler(targetAngle, 0f, 0f);
            suspension.localRotation = Quaternion.Lerp(suspension.localRotation, targetRotation, Time.deltaTime * suspensionDamping);
        }
        else
        {
            // No ground detected, reset suspension to neutral
            Quaternion neutralRotation = Quaternion.Euler(0f, 0f, 0f);
            suspension.localRotation = Quaternion.Lerp(suspension.localRotation, neutralRotation, Time.deltaTime * suspensionDamping);
        }
    }

    // Adjust the rear-left suspension dynamically
    public void AdjustRearLeftSuspension()
    {
        RaycastHit middleHit, rearHit;

        // Cast rays for both the middle and rear left wheels
        bool middleHitDetected = Physics.Raycast(middleLeftWheel.position, Vector3.down, out middleHit, raycastDistance);
        bool rearHitDetected = Physics.Raycast(rearLeftWheel.position, Vector3.down, out rearHit, raycastDistance);

        float middleDistance = middleHitDetected ? middleHit.distance : raycastDistance;
        float rearDistance = rearHitDetected ? rearHit.distance : raycastDistance;

        // Calculate the difference in distances
        float distanceDifference = middleDistance - rearDistance;

        // Determine target angle for the suspension
        float targetAngle = Mathf.Clamp(distanceDifference / raycastDistance * suspensionMaxAngle, -suspensionMaxAngle, suspensionMaxAngle);

        // Smoothly rotate the suspension to balance the wheels
        Quaternion targetRotation = Quaternion.Euler(targetAngle, 0f, 0f);
        rearLeftSuspension.localRotation = Quaternion.Lerp(rearLeftSuspension.localRotation, targetRotation, Time.deltaTime * suspensionDamping);
    }

    // Adjust the rear-right suspension dynamically
    public void AdjustRearRightSuspension()
    {
        RaycastHit middleHit, rearHit;

        // Cast rays for both the middle and rear right wheels
        bool middleHitDetected = Physics.Raycast(middleRightWheel.position, Vector3.down, out middleHit, raycastDistance);
        bool rearHitDetected = Physics.Raycast(rearRightWheel.position, Vector3.down, out rearHit, raycastDistance);

        float middleDistance = middleHitDetected ? middleHit.distance : raycastDistance;
        float rearDistance = rearHitDetected ? rearHit.distance : raycastDistance;

        // Calculate the difference in distances
        float distanceDifference = middleDistance - rearDistance;

        // Determine target angle for the suspension
        float targetAngle = Mathf.Clamp(distanceDifference / raycastDistance * suspensionMaxAngle, -suspensionMaxAngle, suspensionMaxAngle);

        // Smoothly rotate the suspension to balance the wheels
        Quaternion targetRotation = Quaternion.Euler(targetAngle, 0f, 0f);
        rearRightSuspension.localRotation = Quaternion.Lerp(rearRightSuspension.localRotation, targetRotation, Time.deltaTime * suspensionDamping);
    }
}
