using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelController : MonoBehaviour
{
    private float horizontalInput, verticalInput;
    private float currentSteerAngle, currentBrakeForce;
    private bool isBraking;
    private Rigidbody rb;

    // Settings
    [SerializeField] private float motorForce, brakeForce, maxSteerAngle, maxSpeed;
    [SerializeField] private float frictionCoefficient = 0.01f; // Friction value to simulate drag
    [SerializeField] private float idleBrakeForce = 50f; // Brake force when no input is given

    // Wheel Colliders
    [SerializeField] private WheelCollider frontLeftWheelCollider, frontRightWheelCollider;
    [SerializeField] private WheelCollider middleLeftWheelCollider, middleRightWheelCollider;
    [SerializeField] private WheelCollider rearLeftWheelCollider, rearRightWheelCollider;

    // Wheels Transforms
    [SerializeField] private Transform frontLeftWheelTransform, frontRightWheelTransform;
    [SerializeField] private Transform middleLeftWheelTransform, middleRightWheelTransform;
    [SerializeField] private Transform rearLeftWheelTransform, rearRightWheelTransform;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        GetInput();
        if (isBraking)
        {
            StopVehicle(); // Stop vehicle if space is pressed
        }
        else
        {
            HandleMotor();
            HandleSteering();
            ApplyFrictionAndIdleBraking(); // Apply friction/idle brake force
        }
        UpdateWheels();
    }

    private void GetInput()
    {
        // Steering Input
        horizontalInput = Input.GetAxis("Horizontal");

        // Acceleration Input
        verticalInput = Input.GetAxis("Vertical");

        // Braking Input
        isBraking = Input.GetKey(KeyCode.Space);
    }

    private void HandleMotor()
    {
        float forwardSpeed = Vector3.Dot(rb.velocity, transform.forward); // Get the forward speed

        // Check if the vehicle is moving forward, and "S" is pressed (verticalInput < 0)
        if (verticalInput < 0 && forwardSpeed > 0.1f)
        {
            // Treat as braking when "S" is pressed and the vehicle is moving forward
            currentBrakeForce = brakeForce;
            SetMotorTorqueToZero();
            ApplyBraking();
        }
        else
        {
            // Otherwise, handle normal acceleration and braking
            if (rb.velocity.magnitude < maxSpeed)
            {
                // Apply motor force to all wheels except the front steering wheels
                frontLeftWheelCollider.motorTorque = verticalInput * motorForce;
                frontRightWheelCollider.motorTorque = verticalInput * motorForce;
                middleLeftWheelCollider.motorTorque = verticalInput * motorForce;
                middleRightWheelCollider.motorTorque = verticalInput * motorForce;
                rearLeftWheelCollider.motorTorque = verticalInput * motorForce;
                rearRightWheelCollider.motorTorque = verticalInput * motorForce;
            }
            else
            {
                // Prevent further acceleration if at top speed
                SetMotorTorqueToZero();
            }

            // Calculate brake force
            currentBrakeForce = isBraking ? brakeForce : 0f;
            ApplyBraking();
        }
    }


    private void ApplyBraking()
    {
        // Apply brake torque to all wheels
        frontRightWheelCollider.brakeTorque = currentBrakeForce;
        frontLeftWheelCollider.brakeTorque = currentBrakeForce;
        middleLeftWheelCollider.brakeTorque = currentBrakeForce;
        middleRightWheelCollider.brakeTorque = currentBrakeForce;
        rearLeftWheelCollider.brakeTorque = currentBrakeForce;
        rearRightWheelCollider.brakeTorque = currentBrakeForce;
    }

    private void HandleSteering()
    {
        // Steer only the front wheels
        currentSteerAngle = maxSteerAngle * horizontalInput;
        frontLeftWheelCollider.steerAngle = currentSteerAngle;
        frontRightWheelCollider.steerAngle = currentSteerAngle;
    }

    private void ApplyFrictionAndIdleBraking()
    {
        // Reduce speed gradually by applying a frictional force proportional to the current velocity
        Vector3 velocity = rb.velocity;
        rb.AddForce(-velocity * frictionCoefficient, ForceMode.Force);

        // Apply additional braking force when no input is given
        if (Mathf.Abs(verticalInput) < 0.1f && Mathf.Abs(horizontalInput) < 0.1f && rb.velocity.magnitude > 0.1f)
        {
            // Apply a brake force if there is no input and the vehicle is still moving
            currentBrakeForce = idleBrakeForce;
            ApplyBraking();
        }
    }

    private void UpdateWheels()
    {
        // Update positions and rotations for all wheels
        UpdateSingleWheel(frontLeftWheelCollider, frontLeftWheelTransform);
        UpdateSingleWheel(frontRightWheelCollider, frontRightWheelTransform);
        UpdateSingleWheel(middleLeftWheelCollider, middleLeftWheelTransform);
        UpdateSingleWheel(middleRightWheelCollider, middleRightWheelTransform);
        UpdateSingleWheel(rearLeftWheelCollider, rearLeftWheelTransform);
        UpdateSingleWheel(rearRightWheelCollider, rearRightWheelTransform);
    }

    private void UpdateSingleWheel(WheelCollider wheelCollider, Transform wheelTransform)
    {
        Vector3 pos;
        Quaternion rot;
        wheelCollider.GetWorldPose(out pos, out rot);
        wheelTransform.rotation = rot;
        wheelTransform.position = pos;
    }

    private void StopVehicle()
    {
        // Stop the Rigidbody's velocity and angular velocity
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

        // Set motor torque to zero for all wheels to stop their movement
        SetMotorTorqueToZero();

        // Apply maximum brake force to all wheels to ensure vehicle stops completely
        currentBrakeForce = brakeForce;
        ApplyBraking();
    }

    private void SetMotorTorqueToZero()
    {
        // Set motor torque to zero for all wheels
        frontLeftWheelCollider.motorTorque = 0;
        frontRightWheelCollider.motorTorque = 0;
        middleLeftWheelCollider.motorTorque = 0;
        middleRightWheelCollider.motorTorque = 0;
        rearLeftWheelCollider.motorTorque = 0;
        rearRightWheelCollider.motorTorque = 0;
    }
}
