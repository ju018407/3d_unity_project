using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minimap : MonoBehaviour
{

    public Transform rover;

    public int cam = 1;

    private void LateUpdate()
    {
        if (cam  == 1)
        {
            Vector3 newPosition = rover.position;
            newPosition.y = transform.position.y;
            transform.position = newPosition;

            transform.rotation = Quaternion.Euler(90f, rover.eulerAngles.y, 0f);
        }

        else
        {
            Vector3 newPosition = rover.position;
            newPosition.y = transform.position.y;
            transform.position = newPosition;
        }
    }
}
