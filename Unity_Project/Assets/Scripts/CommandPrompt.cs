using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using System.Collections.Generic;


public class CommandPrompt : MonoBehaviour
{
    public TMP_InputField inputField;       // Reference to the input field
    public TMP_Text logText;
    public GameObject panel;                // Reference to the UI Panel
    public RockerBogieSuspension rover; // Reference to your rover script
    public RoverArmControl roverArm;        // Reference to the Rover Arm script

    private string initialLog = "[SYSTEM] Initializing systems...\n[SYSTEM] Boot sequence initiated.\n[SYSTEM] Powering up core modules...\n";
    public static bool isInputActive = false; // Tracks whether input is active
    private bool pToggle = true;

    private bool ignoreSubmit = false;
    private bool errorsent1;
    private bool errorsent2;

    void Start()
    {
        inputField.onEndEdit.AddListener(ExecuteCommand); // Listen for Enter key
        inputField.interactable = false; // Start with input field inactive
        logText.text = initialLog; // Set the initial log text
    }

    void Update()
    {
        // Detect Tab key press
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (pToggle)
            {
                if (roverArm == null || !roverArm.isRunning) // Check if rover arm is done or not set
                {
                    if (EventSystem.current.currentSelectedGameObject == inputField.gameObject)
                    {
                        ignoreSubmit = true;
                        EventSystem.current.SetSelectedGameObject(null); // Unfocus the input field
                    }
                    ToggleInputField();
                }
                else
                {
                    logText.text += "Cannot input commands while Sampling in Process\n";
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            if (pToggle && !isInputActive)
            {
                inputField.gameObject.SetActive(false);
                logText.gameObject.SetActive(false);
                panel.gameObject.SetActive(false);
            }
            if (!pToggle)
            {
                inputField.gameObject.SetActive(true);
                logText.gameObject.SetActive(true);
                panel.gameObject.SetActive(true);
            }
            pToggle = !pToggle;
        }

        if (CameraSwitcher.Instance != null && CameraSwitcher.Instance.roverHealth <= 0 && !errorsent1)
        {
            logText.text += "[Error] Rover health at 0%. SYSTEM FAILURE. SHUTTING DOWN \n";
            errorsent1 = true;
        }

        if (CameraSwitcher.Instance != null && CameraSwitcher.Instance.roverHealth <= 50 && !errorsent2)
        {
            logText.text += "[INFO] Rover health is below 50%. Please operate carefully  \n";
            errorsent2 = true;
        }

    }

    void ToggleInputField()
    {
        isInputActive = !isInputActive;
        inputField.interactable = isInputActive;

        if (isInputActive)
        {
            inputField.ActivateInputField(); // Focus the input field
        }
        else
        {
            inputField.DeactivateInputField(); // Remove focus from the input field
        }
    }

    void ExecuteCommand(string command)
    {
        if (ignoreSubmit)
        {
            ignoreSubmit = false;
            // Do not clear the text, so the command remains.
            return;
        }


        if (string.IsNullOrWhiteSpace(command)) return;

        // Log the command directly
        logText.text += $"> {command}\n";

        // Execute the command
        ParseCommand(command);

        // Clear the input field for the next command
        inputField.text = "";
        inputField.ActivateInputField(); // Focus the input field
    }

    void ParseCommand(string command)
    {
        // Example command parsing logic
        command = command.ToLower();

        if (command.StartsWith("move to "))
        {
            string regionName = command.Substring("Move to ".Length).Trim();
            Vector3 target;
            if (RegionManager.Instance.TryGetDestination(regionName, out target))
            {
                if (CameraSwitcher.Instance != null && CameraSwitcher.Instance.roverHealth <= 0)
                {
                    logText.text += $"[ERROR] System has shut down\n";
                }

                else
                {
                    RoverController.Instance.SetTarget(target);
                    logText.text += $"[INFO] Navigating to {regionName}\n";
                }
            }

            else
            {
                logText.text += $"[ERROR] Unknown region: {regionName}\n";
            }
        }

        else if (command == "terminate journey" || command == "cancel journey" || command == "c")
        {
            if (!RoverController.Instance.autoNavigate)
            {
                logText.text += "[Error] Auto-navigation not in progress.\n";
            }

            else
            {
                logText.text += "[INFO] Auto-navigation cancelled.\n";

            }
            RoverController.Instance.autoNavigate = false;
            // Optionally, stop the rover immediately by zeroing its velocity:
            Rigidbody rb = RoverController.Instance.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.velocity = Vector3.zero;
            }
        }

        else if (command == "t" || command == "pending tasks")
        {
            // Output the pending regions along with cycle info.
            logText.text += RegionTaskManager.Instance.GetPendingTasks();
        }
        // New command to set the total cycles.
        else if (command.StartsWith("target ") && command.Contains("cycles"))
        {
            // Example command: "target 5 cycles"
            // Parse the cycle count from the command.
            string[] parts = command.Split(' ');
            int newCycleCount = 0;
            foreach (string part in parts)
            {
                if (int.TryParse(part, out newCycleCount))
                {
                    break;
                }
            }
            if (newCycleCount > 0)
            {
                RegionTaskManager.Instance.SetTotalCycles(newCycleCount);
                logText.text += "[INFO] Total cycles set to " + newCycleCount + "\n";
            }
            else
            {
                logText.text += "[ERROR] Invalid cycle count.\n";
            }
        }

        else
        {
            logText.text += $"Unknown command: {command}\n";
        }
    }
}
