using UnityEngine;
using UnityEngine.UI;

public class MinimapController : MonoBehaviour
{
    public BathymetricMapGenerator mapGenerator;
    public RawImage minimapImage;
    public Transform roverTransform; // Reference to the rover's transform
    public float minimapRadius = 50f; // Radius of the area to show around the rover

    void Update()
    {
        if (roverTransform == null || mapGenerator == null || minimapImage == null)
            return;

        float sphereRadius = 3f; // Adjust based on your sphere's size
        Vector3 spherePosition = roverTransform.position; // Assuming the sphere is at the rover's position

        Texture2D bathymetricTexture = mapGenerator.GenerateBathymetricTextureAroundPoint(
            roverTransform.position,
            minimapRadius,
            spherePosition,
            sphereRadius
        );

        minimapImage.texture = bathymetricTexture;
    }

}