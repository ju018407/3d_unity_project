using UnityEngine;
using UnityEngine.UI; // Make sure to include this for UI components
using TMPro;

public class CameraSwitcher : MonoBehaviour
{
    public static CameraSwitcher Instance; // Static reference

    public GameObject camera1;  // First camera GameObject
    public GameObject camera2;  // Second camera GameObject
    public GameObject camera3;  // Third camera GameObject
    public GameObject roverCam1;  // Rover Cam1
    public GameObject roverCam2;  // Rover Cam2
    public GameObject roverCam3;  // Rover Cam3
    public Camera minimapCamera;  // Minimap camera GameObject
    public Camera radarCamera;  // Minimap camera GameObject
    public RawImage minimapRawImage;  // Raw Image displaying the minimap
    public RawImage radarRawImage;  // Raw Image displaying the minimap
    public RawImage radarbackgroundRawImage;  // Raw Image displaying the minimap
    public RawImage BathymetricMapRawImage;  // Raw Image displaying the minimap
    public float rotationSpeed = 50f;  // Speed of rotation
    public float minRotationZCam1 = -150f;  // Min Z-axis rotation angle for Cam1
    public float maxRotationZCam1 = 150f;   // Max Z-axis rotation angle for Cam1
    public float minRotationZCam2 = -110f;  // Min Z-axis rotation angle for Cam2
    public float maxRotationZCam2 = -50f;   // Max Z-axis rotation angle for Cam2
    public float minRotationZCam3 = 50f;    // Min Z-axis rotation angle for Cam3
    public float maxRotationZCam3 = 110f;   // Max Z-axis rotation angle for Cam3

    public RawImage image;   // First Raw Image
    public RawImage image1;  // Second Raw Image
    public RawImage image2;  // Third Raw Image
    public RawImage image3;  // Fourth Raw Image

    private int activeCameraIndex = 0;  // 0 for camera1, 1 for camera2, 2 for camera3
    private GameObject[] cameras;
    private GameObject[] roverCams;
    private int visibilityState = 0; // State to track visibility changes
    private int toggleState = 0; // State to track visibility changes
    private bool isMinimapEnabled = true; // State for minimap toggling

    private bool pToggleState = true;
    private int pToggleCams = 0;
    private int pToggleMiniMap = 0;

    public TMP_Text systemStatusText; // Reference to the system status window text
    public GameObject panel;                // Reference to the UI Panel
    public GameObject panel1;                // Reference to the UI Panel

    private Rigidbody roverRigidbody; // For tracking rover speed
    private float roverSpeed = 0f;     // Store rover speed
    private string roverStatus = "Idle"; // Rover status: Idle/Stationary
    public GameObject rover;    // Rover GameObject (to track speed)

    private float displayedRoverSpeed = 0f; // Smoothed speed for display
    private float speedSmoothing = 0.1f;    // Adjust smoothing sensitivity
    private const float SPEED_CONVERSION_FACTOR = 3.6f; // Convert m/s to km/h (or adjust as needed)

    private float currentTemperature = 5f; // Initial temperature
    private float targetTemperature = 5f;  // The temperature we want to reach
    private float temperatureChangeSpeed = 0.1f; // Speed of temperature change
    private float temperatureUpdateInterval = 5f; // Time (in seconds) between temperature changes
    private float temperatureTimer = 0f; // Timer to track temperature updates

    public float roverHealth = 100f;

    public GameObject lightsParent;


    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        cameras = new GameObject[] { camera1, camera3, camera2 };
        roverCams = new GameObject[] { roverCam1, roverCam3, roverCam2 };

        ActivateCamera(0);
        SetInitialVisibility();
        roverRigidbody = rover.GetComponent<Rigidbody>();
        if (roverRigidbody == null)
        {
            Debug.LogError("Rover Rigidbody is missing. Please attach a Rigidbody to the Rover GameObject.");
        }

        UpdateSystemStatus(); // Ensure initial status is displayed correctly
    }


    void Update()
    {

        if (CommandPrompt.isInputActive) return;

        if (Input.GetKeyDown(KeyCode.H))
        {
            if (lightsParent != null)
            {
                lightsParent.SetActive(!lightsParent.activeSelf);
            }
        }

        UpdateRoverSpeed();
        UpdateTemperature(); // Add this method to handle temperature updates
        UpdateSystemStatus();

        // Switch cameras with "V" key
        if (Input.GetKeyDown(KeyCode.V))
        {
            if (pToggleState)
            {
                SwitchCameras();
                ToggleImageVisibility();
            }
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            pToggleState = !pToggleState;
            if (!pToggleState)
            {
                image.gameObject.SetActive(false);
                image1.gameObject.SetActive(false);
                image2.gameObject.SetActive(false);
                image3.gameObject.SetActive(false);
                minimapRawImage.gameObject.SetActive(false);
                radarRawImage.gameObject.SetActive(false);
                radarbackgroundRawImage.gameObject.SetActive(false);
                BathymetricMapRawImage.gameObject.SetActive(false);
                panel.gameObject.SetActive(false);
                panel1.gameObject.SetActive(false);
            }

            else
            {
                pToggleCamsVisibility();
                pToggleMiniMapVisibility();
                panel.gameObject.SetActive(true);
                panel1.gameObject.SetActive(true);
            }
        }

        // Rotate the rover with "Q" (left) or "E" (right) keys
        if (Input.GetKey(KeyCode.Q))
        {
            RotateRoverCam(-rotationSpeed);
        }

        if (Input.GetKey(KeyCode.E))
        {
            RotateRoverCam(rotationSpeed);
        }

        // Toggle minimap camera and Raw Image with "R" key
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (pToggleState)
            {
                ToggleMinimap();
            }
        }
    }

    void UpdateRoverSpeed()
    {
        if (roverRigidbody != null)
        {
            // Calculate actual rover speed (m/s) and convert to display unit (e.g., km/h)
            float currentSpeed = roverRigidbody.velocity.magnitude * SPEED_CONVERSION_FACTOR;

            // Smooth speed using Lerp for gradual change
            displayedRoverSpeed = Mathf.Lerp(displayedRoverSpeed, currentSpeed, speedSmoothing);

            // Update rover status based on smoothed speed
            roverStatus = displayedRoverSpeed > 0.1f ? "Stationary" : "Idle";
        }
        else
        {
            displayedRoverSpeed = 0f;
            roverStatus = "Unknown";
        }
    }

    void UpdateTemperature()
    {
        // Increment the timer
        temperatureTimer += Time.deltaTime;

        // Check if it's time to update the target temperature
        if (temperatureTimer >= temperatureUpdateInterval)
        {
            targetTemperature = Random.Range(-10f, 5f); // Random new target temperature
            temperatureTimer = 0f; // Reset timer
        }

        // Gradually change currentTemperature towards targetTemperature
        currentTemperature = Mathf.Lerp(currentTemperature, targetTemperature, temperatureChangeSpeed * Time.deltaTime);
    }


    void UpdateSystemStatus()
    {
        string camera1Status = (activeCameraIndex == 0) ? "Active" : "Inactive";
        string camera2Status = (activeCameraIndex == 1) ? "Active" : "Inactive";
        string camera3Status = (activeCameraIndex == 2) ? "Active" : "Inactive";

        string miniMapStatus = (minimapCamera.gameObject.activeSelf) ? "Active" : "Inactive";
        string radarMapStatus = (radarCamera.gameObject.activeSelf) ? "Active" : "Inactive";
        string bathymetricMapStatus = (BathymetricMapRawImage.gameObject.activeSelf) ? "Active" : "Inactive";
        string lightStatus = (lightsParent.gameObject.activeSelf) ? "Active" : "Inactive";

        systemStatusText.text =
            $"System Status Window\n\n" +
            $"Rover Speed: {displayedRoverSpeed:F1} km/h\n" + // Display smoothed speed
            $"Rover Status: {roverStatus}\n" +
            $"Rover Health: {roverHealth}%\n" +
            $"Rover Lights: {lightStatus}\n" +
            $"Signal Strength: Strong\n" +
            $"Temperature: {currentTemperature:F1}�C\n" + // Display smoothed temperature
            $"Camera 1 Control: {camera1Status}\n" +
            $"Camera 2 Control: {camera2Status}\n" +
            $"Camera 3 Control: {camera3Status}\n" +
            $"Ingenuity Mini-map: {miniMapStatus}\n" +
            $"Radar Map: {radarMapStatus}\n" +
            $"Bathymetric Map: {bathymetricMapStatus}\n" +
            $"Battery Level: Nominal ";
    }


    void SwitchCameras()
    {
        activeCameraIndex = (activeCameraIndex + 1) % cameras.Length;
        ActivateCamera(activeCameraIndex);
        UpdateSystemStatus(); // Update the system status window after switching cameras
    }


    void ActivateCamera(int index)
    {
        // Enable the selected camera's GameObject and disable the others
        for (int i = 0; i < cameras.Length; i++)
        {
            cameras[i].SetActive(i == index);
        }
    }

    void RotateRoverCam(float rotationDirection)
    {
        GameObject activeRoverCam = roverCams[activeCameraIndex];
        float minRotation = GetMinRotationZ();
        float maxRotation = GetMaxRotationZ();

        // Invert the rotation direction for camera3
        if (activeCameraIndex == 2)
        {
            rotationDirection = -rotationDirection; // Invert the rotation for camera 3
        }

        // Apply rotation
        activeRoverCam.transform.Rotate(0, 0, rotationDirection * Time.deltaTime);

        // Clamp rotation based on the active camera
        ClampRotation(activeRoverCam, minRotation, maxRotation);
    }

    float GetMinRotationZ()
    {
        // Return the minimum Z-axis rotation based on the active camera index
        switch (activeCameraIndex)
        {
            case 0: return minRotationZCam1;
            case 1: return minRotationZCam2;
            case 2: return minRotationZCam3;
            default: return 0;
        }
    }

    float GetMaxRotationZ()
    {
        // Return the maximum Z-axis rotation based on the active camera index
        switch (activeCameraIndex)
        {
            case 0: return maxRotationZCam1;
            case 1: return maxRotationZCam2;
            case 2: return maxRotationZCam3;
            default: return 0;
        }
    }

    void ClampRotation(GameObject roverCam, float minRotationZ, float maxRotationZ)
    {
        // Get current Z rotation and normalize between -180 to 180 degrees
        float zRotation = roverCam.transform.localEulerAngles.z;
        if (zRotation > 180)
        {
            zRotation -= 360;
        }

        // Ensure min and max are also in the same range (-180 to 180)
        if (minRotationZ > 180)
        {
            minRotationZ -= 360;
        }
        if (maxRotationZ > 180)
        {
            maxRotationZ -= 360;
        }

        // Clamp the Z rotation between specified bounds
        zRotation = Mathf.Clamp(zRotation, minRotationZ, maxRotationZ);

        // Apply clamped rotation back to the rover cam
        roverCam.transform.localEulerAngles = new Vector3(
            roverCam.transform.localEulerAngles.x,
            roverCam.transform.localEulerAngles.y,
            zRotation
        );
    }

    void SetInitialVisibility()
    {
        // Initially set image and image1 to visible, image2 and image3 to invisible
        image.gameObject.SetActive(true);
        image1.gameObject.SetActive(true);
        image2.gameObject.SetActive(false);
        image3.gameObject.SetActive(false);
    }

    void pToggleCamsVisibility()
    {
        switch (pToggleCams)
        {
            case 0: // Default state
                image.gameObject.SetActive(true);
                image1.gameObject.SetActive(true);
                image2.gameObject.SetActive(false);
                image3.gameObject.SetActive(false);
                break;
            case 1: // First toggle
                image.gameObject.SetActive(false);
                image1.gameObject.SetActive(true);
                image2.gameObject.SetActive(true);
                image3.gameObject.SetActive(false);
                break;
            case 2: // Second toggle
                image.gameObject.SetActive(true);
                image1.gameObject.SetActive(false);
                image2.gameObject.SetActive(false);
                image3.gameObject.SetActive(true);
                break;
            case 3: // Third toggle
                image.gameObject.SetActive(true);
                image1.gameObject.SetActive(true);
                image2.gameObject.SetActive(false);
                image3.gameObject.SetActive(false);
                break;
        }
    }

    void pToggleMiniMapVisibility()
    {
        switch (pToggleMiniMap)
        {
            case 0: // Minimap enabled, radar disabled
                if (minimapCamera != null)
                {
                    minimapCamera.gameObject.SetActive(true);
                    radarCamera.gameObject.SetActive(false);
                }
                if (minimapRawImage != null)
                {
                    minimapRawImage.gameObject.SetActive(true);
                    radarRawImage.gameObject.SetActive(false);
                    radarbackgroundRawImage.gameObject.SetActive(false);
                    BathymetricMapRawImage.gameObject.SetActive(false);
                }
                break;

            case 1: // Radar enabled, minimap disabled
                if (minimapCamera != null)
                {
                    minimapCamera.gameObject.SetActive(false);
                    radarCamera.gameObject.SetActive(true);
                }
                if (minimapRawImage != null)
                {
                    minimapRawImage.gameObject.SetActive(false);
                    radarRawImage.gameObject.SetActive(true);
                    radarbackgroundRawImage.gameObject.SetActive(true);
                    BathymetricMapRawImage.gameObject.SetActive(false);
                }
                break;

            case 2: // Both disabled (or some other custom state)
                if (minimapCamera != null)
                {
                    minimapCamera.gameObject.SetActive(false);
                    radarCamera.gameObject.SetActive(false);
                }
                if (minimapRawImage != null)
                {
                    minimapRawImage.gameObject.SetActive(false);
                    radarRawImage.gameObject.SetActive(false);
                    radarbackgroundRawImage.gameObject.SetActive(false);
                    BathymetricMapRawImage.gameObject.SetActive(true);
                    //toggleState = 0;
                }
                break;

            default:
                Debug.LogError("Unexpected minimap toggle state");
                break;
        }
    }

    void ToggleImageVisibility()
    {
        // Toggle visibility based on the visibility state
        visibilityState = (visibilityState + 1) % 4;

        switch (visibilityState)
        {
            case 0: // Default state
                pToggleCams = 0;
                image.gameObject.SetActive(true);
                image1.gameObject.SetActive(true);
                image2.gameObject.SetActive(false);
                image3.gameObject.SetActive(false);
                break;
            case 1: // First toggle
                pToggleCams = 1;
                image.gameObject.SetActive(false);
                image1.gameObject.SetActive(true);
                image2.gameObject.SetActive(true);
                image3.gameObject.SetActive(false);
                break;
            case 2: // Second toggle
                pToggleCams = 2;
                image.gameObject.SetActive(true);
                image1.gameObject.SetActive(false);
                image2.gameObject.SetActive(false);
                image3.gameObject.SetActive(true);
                break;
            case 3: // Third toggle
                pToggleCams = 3;
                image.gameObject.SetActive(true);
                image1.gameObject.SetActive(true);
                image2.gameObject.SetActive(false);
                image3.gameObject.SetActive(false);
                visibilityState = 0;
                break;
        }
    }

    void ToggleMinimap()
    {
        // Increment the toggle state and loop back to 0 if exceeding 2 (for three states)
        toggleState = (toggleState + 1) % 3;

        switch (toggleState)
        {
            case 0: // Minimap enabled, radar disabled
                pToggleMiniMap = 0;
                if (minimapCamera != null)
                {
                    minimapCamera.gameObject.SetActive(true);
                    radarCamera.gameObject.SetActive(false);
                }
                if (minimapRawImage != null)
                {
                    minimapRawImage.gameObject.SetActive(true);
                    radarRawImage.gameObject.SetActive(false);
                    radarbackgroundRawImage.gameObject.SetActive(false);
                    BathymetricMapRawImage.gameObject.SetActive(false);
                }
                break;

            case 1: // Radar enabled, minimap disabled
                pToggleMiniMap = 1;
                if (minimapCamera != null)
                {
                    minimapCamera.gameObject.SetActive(false);
                    radarCamera.gameObject.SetActive(true);
                }
                if (minimapRawImage != null)
                {
                    minimapRawImage.gameObject.SetActive(false);
                    radarRawImage.gameObject.SetActive(true);
                    radarbackgroundRawImage.gameObject.SetActive(true);
                    BathymetricMapRawImage.gameObject.SetActive(false);
                }
                break;

            case 2: // Both disabled (or some other custom state)
                pToggleMiniMap = 2;
                if (minimapCamera != null)
                {
                    minimapCamera.gameObject.SetActive(false);
                    radarCamera.gameObject.SetActive(false);
                }
                if (minimapRawImage != null)
                {
                    minimapRawImage.gameObject.SetActive(false);
                    radarRawImage.gameObject.SetActive(false);
                    radarbackgroundRawImage.gameObject.SetActive(false);
                    BathymetricMapRawImage.gameObject.SetActive(true);
                    //toggleState = 0;
                }
                break;

            default:
                Debug.LogError("Unexpected minimap toggle state");
                break;
        }

        UpdateSystemStatus(); // Reflect minimap and radar changes

    }

}
