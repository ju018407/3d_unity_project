﻿using UnityEngine;
using TMPro;

public class MarsClock : MonoBehaviour
{
    [Header("References")]
    // Reference to the directional light that’s running the day-night cycle.
    public DayNightCycle dayNightCycle;
    // UI Text element to display the clock.
    public TMP_Text clockDisplay;
    // UI Text element to display the day counter.
    public TMP_Text dayCounterDisplay;

    [Header("Mars Day Settings")]
    // Length of a Mars sol in seconds (24h 39m 35s ≈ 88775 seconds).
    public float marsSolSeconds = 88775f;

    [Header("Time Settings")]
    // The time that corresponds to the light's starting rotation.
    // For example, if the light's initial rotation (x-axis) is 17.265 and that should be 10:00:
    public int startHour = 10;
    public int startMinute = 0;
    public int startSecond = 0;

    // Internal simulation time variables
    // totalSimTime accumulates all simulation seconds (not modded)
    private float totalSimTime = 0f;
    // simulationTimeSeconds is the modded time that corresponds to the current clock display.
    private float simulationTimeSeconds;

    void Update()
    {
        // Calculate how many degrees the light rotates this frame.
        float deltaDegrees = dayNightCycle.rotationSpeed * Time.deltaTime;
        // Convert degrees rotated into simulation seconds.
        float deltaSimSeconds = (deltaDegrees / 360f) * marsSolSeconds;
        // Accumulate the total simulation seconds.
        totalSimTime += deltaSimSeconds;

        // Calculate the starting offset (10:00 in seconds).
        float offset = startHour * 3600 + startMinute * 60 + startSecond;
        // Calculate current simulation time modulo a Mars sol.
        simulationTimeSeconds = (offset + totalSimTime) % marsSolSeconds;

        // Compute day counter.
        // The day counter increments by 1 every time totalSimTime passes a full sol.
        int dayCounter = Mathf.FloorToInt(totalSimTime / marsSolSeconds);

        // Convert simulationTimeSeconds into hours, minutes, seconds.
        // Use modulo 24 for hours to keep a standard 24-hour clock display.
        int hours = Mathf.FloorToInt(simulationTimeSeconds / 3600f) % 24;
        int minutes = Mathf.FloorToInt((simulationTimeSeconds % 3600f) / 60f);
        int seconds = Mathf.FloorToInt(simulationTimeSeconds % 60f);

        // Update the UI clock display.
        clockDisplay.text = string.Format("Martian Time (Landing Site): {0:00}:{1:00}:{2:00}", hours, minutes, seconds);
        // Update the day counter display.
        dayCounterDisplay.text = "Days Passed: " + dayCounter.ToString();
    }
}
