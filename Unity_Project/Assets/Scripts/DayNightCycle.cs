using UnityEngine;

public class DayNightCycle : MonoBehaviour
{
    // Adjust this speed to control how fast the cycle occurs.
    public float rotationSpeed = 5f; 

    void Update()
    {
        // Rotate around the X-axis to simulate the sun's movement.
        transform.Rotate(Vector3.right, rotationSpeed * Time.deltaTime);
    }
}