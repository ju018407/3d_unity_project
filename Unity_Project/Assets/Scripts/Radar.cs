using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radar : MonoBehaviour
{
    public GameObject[] trackedObjects; // Objects to track
    private List<GameObject> radarObjects; // Radar blips near the player
    private List<GameObject> borderObjects; // Radar blips at the radar border
    public GameObject radarPrefab; // Prefab for radar blips
    public float radarRadius = 5f; // Radius of the radar
    public float switchDistance = 10f; // Distance threshold for switching between radar and border

    // Start is called before the first frame update
    void Start()
    {
        createRadarObjects();
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < radarObjects.Count; i++)
        {
            if (trackedObjects[i] == null) continue;

            // Calculate the horizontal distance (ignore Y-axis)
            Vector3 playerPositionXZ = new Vector3(transform.position.x, 0, transform.position.z);
            Vector3 objectPositionXZ = new Vector3(trackedObjects[i].transform.position.x, 0, trackedObjects[i].transform.position.z);
            float distance = Vector3.Distance(objectPositionXZ, playerPositionXZ);

            if (distance > switchDistance)
            {
                // Object is far: position blip at the radar border
                Vector3 direction = (objectPositionXZ - playerPositionXZ).normalized;
                Vector3 borderPosition = playerPositionXZ + direction * radarRadius;

                radarObjects[i].SetActive(false); // Hide the radar blip
                borderObjects[i].SetActive(true); // Show the border blip
                borderObjects[i].transform.position = new Vector3(borderPosition.x, transform.position.y, borderPosition.z); // Keep it on the radar plane
            }
            else
            {
                // Object is near: position blip closer to the player
                radarObjects[i].SetActive(true); // Show the radar blip
                radarObjects[i].transform.position = new Vector3(
                    trackedObjects[i].transform.position.x,
                    transform.position.y,
                    trackedObjects[i].transform.position.z); // Keep it on the radar plane

                borderObjects[i].SetActive(false); // Hide the border blip
            }
        }
    }

    void createRadarObjects()
    {
        radarObjects = new List<GameObject>();
        borderObjects = new List<GameObject>();

        foreach (GameObject obj in trackedObjects)
        {
            // Create radar blip
            GameObject radarBlip = Instantiate(radarPrefab, obj.transform.position, Quaternion.identity);
            radarObjects.Add(radarBlip);

            // Create border blip
            GameObject borderBlip = Instantiate(radarPrefab, obj.transform.position, Quaternion.identity);
            borderBlip.SetActive(false); // Initially hide the border blip
            borderObjects.Add(borderBlip);
        }
    }
}
