using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DistanceTracker : MonoBehaviour
{
    public TMP_Text distanceText;  // Assign your UI Text element in the Inspector
    public float distanceMultiplier = 2f;  // Adjust this to make the displayed distance faster
    private Vector3 lastPosition;
    private float totalDistance;  // Accumulated distance in meters

    void Start()
    {
        lastPosition = transform.position;
    }

    void FixedUpdate()
    {
        // Calculate the distance moved since the last physics update
        float distanceMoved = Vector3.Distance(transform.position, lastPosition);
        totalDistance += distanceMoved;
        lastPosition = transform.position;
    }

    void Update()
    {
        // Multiply the total distance to simulate a faster travel display
        float displayedDistance = totalDistance * distanceMultiplier;
        // Convert from meters to kilometers (if 1 unit = 1 meter)
        float kilometers = displayedDistance / 1000f;
        distanceText.text = "Distance Travelled: " + kilometers.ToString("F2") + " km";
    }
}
