using System.Collections.Generic;
using UnityEngine;
using TMPro;
using static UnityEditor.Timeline.TimelinePlaybackControls;

public class RegionTaskManager : MonoBehaviour
{
    public static RegionTaskManager Instance;

    // List to hold regions that still need scanning for the current cycle.
    public List<string> pendingRegions = new List<string>();

    // Master list of all regions to scan.
    public List<string> allRegions = new List<string>();

    // Number of cycles to perform, default is 3.
    public int totalCycles = 3;
    // Current cycle number.
    public int currentCycle = 1;
    public TMP_Text logText;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            // Initialize the master list of regions.
            allRegions.Add("Hsaka");
            allRegions.Add("Mustafar Heights");
            allRegions.Add("Samarium");
            allRegions.Add("Hellas");
            allRegions.Add("Solis");

            // Start the first cycle.
            ResetCycle();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Resets the pendingRegions list by copying the master list.
    void ResetCycle()
    {
        pendingRegions = new List<string>(allRegions);
        logText.text += "Cycle " + currentCycle + " started. Regions to scan have been reset.\n";
    }

    // Call this method when a region is scanned.
    public void MarkRegionScanned(string regionName)
    {
        if (pendingRegions.Contains(regionName))
        {
            pendingRegions.Remove(regionName);
            logText.text += regionName + " removed from Cycle " + currentCycle + "\n";

            // Check if the current cycle is complete.
            if (pendingRegions.Count == 0)
            {
                logText.text += "Cycle " + currentCycle + " complete.\n";
                if (currentCycle < totalCycles)
                {
                    currentCycle++;
                    ResetCycle();
                }
                else
                {
                    logText.text += "All cycles complete.\n";
                }
            }
        }
    }

    // Returns a string with the current cycle info and pending regions.
    public string GetPendingTasks()
    {
        string info = "Cycle " + currentCycle + " of " + totalCycles + "\n";
        if (pendingRegions.Count == 0)
        {
            if (currentCycle == totalCycles)
            {
                return info + "All cycles have been completed!\n";
            }

            else
            {
                return info + "All regions have been scanned for this cycle!\n";
            }
        }

        string tasks = info + "Regions left to scan:\n";
        foreach (string region in pendingRegions)
        {
            tasks += "- " + region + "\n";
        }
        return tasks;
    }

    // Allows you to change the total number of cycles via command.
    public void SetTotalCycles(int newTotalCycles)
    {
        if (newTotalCycles < 1) return;
        totalCycles = newTotalCycles;
        //currentCycle = 1; // Optionally reset to the first cycle.
        //ResetCycle();
        //Debug.Log("Total cycles set to " + totalCycles + ". Cycles have been reset.");
    }
}
