using UnityEngine;
using System.Collections.Generic;

public class RegionManager : MonoBehaviour
{
    public static RegionManager Instance;

    // Dictionary mapping region names to coordinates
    private Dictionary<string, Vector3> destinations = new Dictionary<string, Vector3>()
    {
        { "hsaka", new Vector3(17, 8, 14) },
        { "mustafar heights", new Vector3(5, 8, -26) },
        { "samarium", new Vector3(-39, 8, 30) },
        { "hellas", new Vector3(-46, 9, 70) },
        { "solis", new Vector3(20, 9, 80) },

        // Add other destinations as needed
    };

    private void Awake()
    {
        Instance = this;
    }

    public bool TryGetDestination(string name, out Vector3 destination)
    {
        return destinations.TryGetValue(name, out destination);
    }
}
