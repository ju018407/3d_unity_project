using UnityEngine;

public class BathymetricMapGenerator : MonoBehaviour
{
    public Terrain terrain;
    public Gradient depthGradient;
    public int minimapResolution = 256; // Configurable resolution for the minimap

    public Texture2D GenerateBathymetricTextureAroundPoint(Vector3 centerPoint, float radius, Vector3 spherePosition, float sphereRadius)
    {
        if (terrain == null)
            return null;

        TerrainData terrainData = terrain.terrainData;
        Vector3 terrainPos = terrain.transform.position;
        Vector3 terrainSize = terrainData.size;

        int heightmapResolution = terrainData.heightmapResolution;
        float[,] heights = terrainData.GetHeights(0, 0, heightmapResolution, heightmapResolution);

        Texture2D texture = new Texture2D(minimapResolution, minimapResolution);

        float worldToHeightmapX = (heightmapResolution - 1) / terrainSize.x;
        float worldToHeightmapZ = (heightmapResolution - 1) / terrainSize.z;

        for (int x = 0; x < minimapResolution; x++)
        {
            for (int y = 0; y < minimapResolution; y++)
            {
                float xRatio = (float)x / (minimapResolution - 1);
                float yRatio = (float)y / (minimapResolution - 1);

                Vector3 worldPos = new Vector3(
                    centerPoint.x - radius + 2 * radius * xRatio,
                    centerPoint.y,
                    centerPoint.z - radius + 2 * radius * yRatio
                );

                int heightmapX = Mathf.Clamp(
                    Mathf.RoundToInt((worldPos.x - terrainPos.x) * worldToHeightmapX),
                    0,
                    heightmapResolution - 1
                );

                int heightmapZ = Mathf.Clamp(
                    Mathf.RoundToInt((worldPos.z - terrainPos.z) * worldToHeightmapZ),
                    0,
                    heightmapResolution - 1
                );

                float depth = 1f - Mathf.Clamp01(heights[heightmapZ, heightmapX]);
                Color depthColor = depthGradient.Evaluate(depth);

                // Check if the world position is within the sphere's radius
                float distanceToSphere = Vector3.Distance(worldPos, spherePosition);
                if (distanceToSphere <= sphereRadius)
                {
                    // Assign a distinct color to represent the sphere
                    depthColor = Color.green;
                }

                texture.SetPixel(x, y, depthColor);
            }
        }

        texture.Apply();
        return texture;
    }

}