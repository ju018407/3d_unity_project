using UnityEngine;
using System.Collections;
using TMPro;

public class RoverArmControl : MonoBehaviour
{
    public float rotationSpeed = 1.0f;  // Speed at which the arm will rotate (degrees per second)
    public float subarmRotationSpeed = 1.0f;  // Speed for the subarm's rotation
    public GameObject arm;              // Reference to the rover arm that will rotate
    public GameObject subarm;           // Reference to the subarm that will rotate on the Y axis
    public GameObject emptyObject_1;    // First empty object that will detect the terrain
    public GameObject emptyObject_2;    // Second empty object that will detect the terrain
    public LayerMask terrainLayer;      // The layer mask for the terrain

    private bool isArmRotating = false;      // To check if the main arm rotation is active
    private bool isSubarmRotating = false;   // To check if the subarm rotation is active
    private bool stopProcess = false;        // To stop the loop when emptyObject_2 touches the terrain
    private float subarmYRotation = 82.0f;   // Starting Y rotation for the subarm
    private bool delay = false;
    private bool done = false;
    public bool isRunning = false;

    private int text1 = 0;
    private int text2 = 0;
    private int text3 = 0;
    private int text4 = 0;

    private Vector3 initialArmRotation;
    private Vector3 initialSubArmRotation;

    public RockerBogieSuspension wheelController;
    private Rigidbody roverRigidbody;
    private float armCumulativeRotation = 0.0f;

    public TMP_Text logText;

    public Collider[] scanningAreas;     // Assign each region's scanning sphere collider here
    public string[] scanningAreaNames;     // The names corresponding to each scanning area


    void Start()
    {
        // Save the initial rotation of the arm
        initialArmRotation = arm.transform.localEulerAngles;
        initialSubArmRotation = subarm.transform.localEulerAngles;

        // Get the Rigidbody component from the rover
        roverRigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        // Check if the 'R' key is pressed and the rover is stationary
        if (!CommandPrompt.isInputActive && Input.GetKey(KeyCode.R) && IsRoverStationary() && !stopProcess && !isArmRotating && !isSubarmRotating)
        {
            logText.text += "Rover Arm Activated...\n";
            logText.text += "Position Locked...\n";
            isArmRotating = true;
            wheelController.enabled = false;
            isRunning = true;
            text4 = 0; // Reset error flag if the rover is stationary
        }

        // Check if the 'R' key is pressed while the rover is moving (error state)
        if (!CommandPrompt.isInputActive && Input.GetKey(KeyCode.R) && !IsRoverStationary() && !stopProcess && !isArmRotating && !isSubarmRotating && text4 == 0)
        {
            logText.text += "Error Rover in Movement...\n";
            text4 = 1; // Prevent multiple error logs
        }

        // Reset error flag when the 'R' key is released
        if (Input.GetKeyUp(KeyCode.R))
        {
            text4 = 0;
        }


        if (isArmRotating && !stopProcess)
        {
            RotateArm();
        }

        CheckTerrainTouch();

        if (isSubarmRotating && !stopProcess)
        {
            RotateSubarm();
        }

        if (stopProcess && !isArmRotating && !delay)
        {
            ReturnArmToInitialPosition();
        }

        if (stopProcess && !isArmRotating && delay)
        {
            StartCoroutine(ReturnArmToInitialPositionwithDelay());
        }

        if (done)
        {
            StopAllCoroutines();
            isArmRotating = false;
            isSubarmRotating = false;
            done = false;
            delay = false;
            wheelController.enabled = true;
            text1 = 0;
            text2 = 0;
            text3 = 0;
            logText.text += "Position Unlocked...\n";
            isRunning = false;

            // Loop through all scanning areas to check if the rover is in one.
            for (int i = 0; i < scanningAreas.Length; i++)
            {
                if (scanningAreas[i] != null && scanningAreas[i].bounds.Contains(transform.position))
                {
                    logText.text += "Scanning completed in " + scanningAreaNames[i] + "\n";
                    // Remove this region from the pending task list.
                    RegionTaskManager.Instance.MarkRegionScanned(scanningAreaNames[i]);
                    break; // Stop after finding the first matching region.
                }
            }
        }



        if (text1 == 1)
        {
            //logText.text += "Sample Collection Successful...\n";
            //logText.text += "Arm Returning Back to initial Position...\n";
            logText.text += "Collecting Samples\n";
            text1++;
        }

        if (text2 == 1)
        {
            logText.text += "Sample Collection Successful...\n";
            logText.text += "Arm Returning Back to initial Position...\n";
            text2++;
        }

        if(text3 == 1)
        {
            logText.text += "Error... Terrain Uneven\n";
            logText.text += "Arm Returning back to initial position\n";
            text3++;
        }
    }

    bool IsRoverStationary()
    {
        return roverRigidbody.velocity.magnitude < 0.01f;
    }

    void RotateArm()
    {
        Vector3 currentRotation = arm.transform.localEulerAngles;

        if (currentRotation.y > 180)
        {
            currentRotation.y -= 360;
        }

        float rotationStep = rotationSpeed * Time.deltaTime;
        armCumulativeRotation += Mathf.Abs(rotationStep);

        currentRotation.y -= rotationStep;
        arm.transform.localEulerAngles = currentRotation;

        if (armCumulativeRotation >= 190.0f)
        {
            isArmRotating = false;
            stopProcess = true;
            return;
        }

        if (IsTouchingTerrain(emptyObject_1) || IsTouchingTerrain(emptyObject_2))
        {
            isArmRotating = false;
            isSubarmRotating = true;
        }
    }

    void CheckTerrainTouch()
    {
        if (IsTouchingTerrain(emptyObject_2))
        {
            stopProcess = true;
            isArmRotating = false;
            isSubarmRotating = false;
            delay = true;
            text1++;
        }
    }

    void RotateSubarm()
    {
        subarmYRotation += subarmRotationSpeed * Time.deltaTime;

        Vector3 currentRotation = subarm.transform.localEulerAngles;
        currentRotation.y = subarmYRotation;
        subarm.transform.localEulerAngles = currentRotation;

        if (!IsTouchingTerrain(emptyObject_1) && !IsTouchingTerrain(emptyObject_2))
        {
            isSubarmRotating = false;
            isArmRotating = true;
        }
    }

    bool IsTouchingTerrain(GameObject emptyObject)
    {
        RaycastHit hit;
        if (Physics.Raycast(emptyObject.transform.position, Vector3.down, out hit, 0.1f, terrainLayer))
        {
            return hit.collider.CompareTag("Terrain");
        }
        return false;
    }

    void ReturnArmToInitialPosition()
    {
        text3++;
        Vector3 currentRotation = arm.transform.localEulerAngles;
        Vector3 targetRotation = initialArmRotation;

        if (Vector3.Distance(currentRotation, targetRotation) > 0.1f)
        {
            currentRotation = Vector3.MoveTowards(currentRotation, targetRotation, rotationSpeed * Time.deltaTime);
            arm.transform.localEulerAngles = currentRotation;
        }
        else
        {
            arm.transform.localEulerAngles = targetRotation;
            armCumulativeRotation = 0.0f;
            ReturnSubArmToInitialPosition();
        }
    }

    // Function to return the arm to its initial position smoothly
    private IEnumerator ReturnArmToInitialPositionwithDelay()
    {
        yield return new WaitForSeconds(7.5f);
        // Lerp the arm back to the initial rotation smoothly
        text2++;
        Vector3 currentRotation = arm.transform.localEulerAngles;
        Vector3 targetRotation = initialArmRotation;

        // Check if the arm is close enough to the initial rotation
        if (Vector3.Distance(currentRotation, targetRotation) > 0.1f)
        {
            // Smoothly rotate the arm back to the initial rotation
            currentRotation = Vector3.MoveTowards(currentRotation, targetRotation, rotationSpeed * Time.deltaTime);
            arm.transform.localEulerAngles = currentRotation;
        }
        else
        {
            // Ensure arm reaches exactly to the initial position
            arm.transform.localEulerAngles = targetRotation;
            armCumulativeRotation = 0.0f; // Reset cumulative rotation
            ReturnSubArmToInitialPosition();
        }
    }

    void ReturnSubArmToInitialPosition()
    {
        Vector3 currentRotation = subarm.transform.localEulerAngles;
        Vector3 targetRotation = initialSubArmRotation;

        if (Vector3.Distance(currentRotation, targetRotation) > 0.1f)
        {
            currentRotation = Vector3.MoveTowards(currentRotation, targetRotation, rotationSpeed * Time.deltaTime);
            subarm.transform.localEulerAngles = currentRotation;
        }
        else
        {
            subarm.transform.localEulerAngles = targetRotation;
            stopProcess = false;
            subarmYRotation = 82.0f;
            done = true;
        }
    }
}
