using TMPro;
using UnityEngine;

public class RoverController : MonoBehaviour
{
    public static RoverController Instance;

    public bool autoNavigate = false;      // Auto mode flag
    public Vector3 targetPosition;           // Destination to navigate toward
    public float stopThreshold = 2f;         // Distance within which the rover stops

    // Obstacle detection parameters
    [Header("Obstacle Detection Settings")]
    public float obstacleDetectionDistance = 5f;  // How far ahead to look for obstacles
    public float detectionConeAngle = 30f;        // Half-angle of the detection cone (e.g., 30� to each side)
    public int numDetectionRays = 5;              // Number of rays to cast across the cone
    public LayerMask obstacleLayers;              // Layers considered as obstacles

    [Header("Avoidance Smoothing Settings")]
    [Tooltip("How quickly to smooth the avoidance steering input.")]
    public float avoidanceSmoothing = 2f;
    [Tooltip("Multiplier to adjust the strength of avoidance steering.")]
    public float avoidanceMultiplier = 1f;

    // Internal variable for smoothing the avoidance input.
    private float smoothedAvoidanceTurn = 0f;

    // References to manual control components
    private RockerBogieSuspension manualControl;
    private Rigidbody rb;

    public TMP_Text logText;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        manualControl = GetComponent<RockerBogieSuspension>();
        rb = GetComponent<Rigidbody>();
    }

    // Called from your command handler to set a new target.
    public void SetTarget(Vector3 target)
    {
        targetPosition = target;
        autoNavigate = true;
    }

    void Update()
    {
        if (CameraSwitcher.Instance != null && CameraSwitcher.Instance.roverHealth <= 0)
        {
            autoNavigate = false;
            return; // Stop processing movement inputs
        }

        // If auto-navigation is active, override manual input:
        if (autoNavigate)
        {
            AutoNavigate();
        }
    }

    void AutoNavigate()
    {
        // Check battery again in case it reached 0 mid-navigation
        if (CameraSwitcher.Instance != null && CameraSwitcher.Instance.roverHealth <= 0)
        {
            autoNavigate = false;
            return;
        }

        // Compute the vector and distance to the target.
        Vector3 toTarget = targetPosition - transform.position;
        float distanceToTarget = toTarget.magnitude;
        Vector3 directionToTarget = toTarget.normalized;

        // Compute the steering input needed to face the target.
        float targetAngle = Vector3.SignedAngle(transform.forward, directionToTarget, Vector3.up);
        float turnInput = Mathf.Clamp(targetAngle / manualControl.steeringAngle, -1f, 1f);
        float moveInput = (distanceToTarget > stopThreshold) ? 1f : 0f;

        // Get the raw avoidance steering from obstacles.
        float rawAvoidanceTurn = GetObstacleAvoidanceTurn();
        // Smooth the avoidance turn so changes are gradual.
        smoothedAvoidanceTurn = Mathf.Lerp(smoothedAvoidanceTurn, rawAvoidanceTurn, Time.deltaTime * avoidanceSmoothing);

        // Blend the target steering and avoidance steering.
        // When there is a strong avoidance signal, we blend more toward it.
        float avoidanceBlend = Mathf.Clamp01(Mathf.Abs(smoothedAvoidanceTurn) * avoidanceMultiplier);
        float finalTurnInput = Mathf.Lerp(turnInput, smoothedAvoidanceTurn * avoidanceMultiplier, avoidanceBlend);

        // Optionally, reduce forward speed during avoidance maneuvers.
        if (avoidanceBlend > 0.1f)
        {
            moveInput = 0.5f;
        }

        // Apply movement and steering.
        ApplyMovement(moveInput, finalTurnInput);

        // Stop auto-navigation when the rover is close enough.
        if (distanceToTarget <= stopThreshold)
        {
            autoNavigate = false;
            logText.text += "[INFO] Arrived at the Destination\n";
        }
    }

    /// <summary>
    /// Casts rays in a cone and counts obstacles on the left and right sides.
    /// Returns:
    ///   0 if no obstacles,
    ///  +1 if left side is more blocked (so steer right),
    ///  -1 if right side is more blocked (so steer left).
    /// </summary>
    float GetObstacleAvoidanceTurn()
    {
        Vector3 rayOrigin = transform.position + Vector3.up * 0.5f;
        float halfAngle = detectionConeAngle;
        int leftHits = 0;
        int rightHits = 0;

        for (int i = 0; i < numDetectionRays; i++)
        {
            float t = (numDetectionRays == 1) ? 0.5f : (float)i / (numDetectionRays - 1);
            float angleOffset = Mathf.Lerp(-halfAngle, halfAngle, t);
            Vector3 rayDirection = Quaternion.Euler(0, angleOffset, 0) * transform.forward;

            Debug.DrawRay(rayOrigin, rayDirection * obstacleDetectionDistance, Color.red);

            if (Physics.Raycast(rayOrigin, rayDirection, out RaycastHit hit, obstacleDetectionDistance, obstacleLayers))
            {
                // Check the angle of the hit surface.
                float surfaceAngle = Vector3.Angle(hit.normal, Vector3.up);
                // If the surface is nearly flat (e.g., less than 15�), it's likely the ground.
                if (surfaceAngle < 15f)
                    continue; // Skip this hit

                if (angleOffset < 0)
                    leftHits++;
                else if (angleOffset > 0)
                    rightHits++;
            }
        }

        if (leftHits == 0 && rightHits == 0)
            return 0f;

        if (leftHits > rightHits)
            return 1f;
        else if (rightHits > leftHits)
            return -1f;
        else
            return 1f;
    }


    void ApplyMovement(float moveInput, float turnInput)
    {
        // Move the rover forward or backward.
        Vector3 movement = transform.forward * moveInput * manualControl.speed * Time.deltaTime;
        rb.MovePosition(rb.position + movement);

        // Rotate the rover for turning.
        if (moveInput != 0 && turnInput != 0)
        {
            float adjustedTurnSpeed = manualControl.turnSpeed * (moveInput < 0 ? -1 : 1);
            Quaternion turnRotation = Quaternion.Euler(0f, turnInput * adjustedTurnSpeed * Time.deltaTime, 0f);
            rb.MoveRotation(rb.rotation * turnRotation);
        }

        // Update wheels and suspension.
        manualControl.RotateWheels(moveInput);
        manualControl.SteerWheels(turnInput);
        manualControl.AdjustSuspension(manualControl.frontLeftSuspension, manualControl.frontLeftWheel);
        manualControl.AdjustSuspension(manualControl.frontRightSuspension, manualControl.frontRightWheel);
        manualControl.AdjustRearLeftSuspension();
        manualControl.AdjustRearRightSuspension();
    }
}
